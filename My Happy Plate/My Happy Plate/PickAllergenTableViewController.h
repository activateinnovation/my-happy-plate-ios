//
//  PickAllergenTableViewController.h
//  My Happy Plate
//
//  Created by JoshJSZ on 6/17/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllAllergon.h"
#import "User.h"
#import "CurrentUserObject.h"

@interface PickAllergenTableViewController : UITableViewController

@property (strong, nonatomic) AllAllergon *allAllergen;
@property (strong, nonatomic) NSMutableArray *localAllAllergen;
@property (strong, nonatomic) User *user;


@end
