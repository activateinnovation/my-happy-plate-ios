//
//  RecoverPasswordTableViewController.h
//  My Happy Plate
//
//  Created by JoshJSZ on 6/24/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "CurrentUserObject.h"

@interface RecoverPasswordTableViewController : UITableViewController

@property (strong, nonatomic) User *user;

@end
