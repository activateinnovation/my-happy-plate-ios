//
//  AllAllergon.h
//  My Happy Plate
//
//  Created by JoshJSZ on 6/13/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AllAllergon : NSObject

@property (strong, nonatomic) NSMutableArray *allAllergonList;

@end
