//
//  CurrentUserObject.m
//  My Happy Plate
//
//  Created by JoshJSZ on 6/13/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "CurrentUserObject.h"

static User* _currentUserObject = nil;

@implementation CurrentUserObject

+ (void)load{
    _currentUserObject = [[User alloc] init];
}

+ (User *)currentUserObject{
    return _currentUserObject;
}

@end
