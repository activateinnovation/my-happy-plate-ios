//
//  AllAllergon.m
//  My Happy Plate
//
//  Created by JoshJSZ on 6/13/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "AllAllergon.h"
#import <Parse/Parse.h>

@implementation AllAllergon

@synthesize allAllergonList = _allAllergonList;

- (NSMutableArray *)allAllergonList
{
    if (!_allAllergonList) {
        _allAllergonList = [[NSMutableArray alloc] init];
    }
        
    PFQuery *query = [PFQuery queryWithClassName:@"AllAllergen"];
    [query whereKeyExists:@"Allergen"];
    NSArray *tmp;
    tmp = [query findObjects];

    for (int x = 0; x < tmp.count; x++) {
        [_allAllergonList addObject:[tmp objectAtIndex:x][@"Allergen"]];
    }
    
    return _allAllergonList;
}

@end
