//
//  User.m
//  My Happy Plate
//
//  Created by JoshJSZ on 6/13/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "User.h"
#import <Parse/Parse.h>

@implementation User

@synthesize email = _email;
@synthesize allergonList = _allergonList;

- (NSString *)email
{
    return _email;
}

//store email in lower case
- (void)setEmail:(NSString *)email{
    email = email.lowercaseString;
    _email = email;
}

- (NSMutableArray *)allergonList
{
    if (!_allergonList) _allergonList = [[NSMutableArray alloc] init];
    
    return _allergonList;
}

-(void)logOut
{
    [PFUser logOut];
}
//- (BOOL)isUserLoggedIn:(NSUserDefaults *)userDefaults{
- (BOOL)isUserLoggedIn{
    BOOL loggedIn=NO;
    
    NSLog(@"Let's see if there is User!");
    //[userDefaults boolForKey:@"loggedIn"] &&
    if ([PFUser currentUser]) {
        loggedIn = YES;
        NSLog(@"There is User!");
    }

    return loggedIn;
}

- (BOOL)logInNewUser
{
    BOOL isUserLoggedIn = NO;
    
    
    if([PFUser logInWithUsername:self.userName password:self.passWord] != nil){
        isUserLoggedIn = YES;
    }
    
    return isUserLoggedIn;
}

- (BOOL)updateEmail
{
    BOOL isEmailUpdated = NO;
    
    if([self isUserLoggedIn]){
        [[PFUser currentUser] setEmail:self.email];
        [[PFUser currentUser] saveEventually];
        isEmailUpdated = YES;
    }
    
    return isEmailUpdated;
}

- (BOOL)regNewUser
{
    BOOL isRegNewUserSuccess = NO;
    
    if (self.userName.length &&
        self.passWord.length &&
        self.firstName.length &&
        self.lastName.length &&
        self.email.length &&
        self.allergonList.count) {
        
        PFUser *newUser = [PFUser user];
        newUser.username = self.userName;
        newUser.email = self.email;
        newUser.password = self.passWord;
        newUser[@"lastName"] = self.lastName;
        newUser[@"firstName"] = self.firstName;
        newUser[@"allergenList"] = self.allergonList;
        
        isRegNewUserSuccess = [newUser signUp];
    }
    
    if ([self isUserLoggedIn]) {
        if (![self isUserVerified]) {
            [self logOut];
        }
    }
    
//    if (isRegNewUserSuccess) {
//        isRegNewUserSuccess = self.logInNewUser;
//    }
    
    return isRegNewUserSuccess;
}

- (void)fetchCurrentUserInfo
{
    PFUser *user = [PFUser currentUser];
    
    self.userName = [user objectForKey:@"username"];
    self.email = [user objectForKey:@"email"];
    self.firstName = [user objectForKey:@"firstName"];
    self.lastName = [user objectForKey:@"lastName"];
}

- (BOOL)isObjInCurrentUser:(NSString*)obj inKey:(NSString*)key;
{
    BOOL isObjInCurrentUser = NO;
    PFUser *user = [PFUser currentUser];
    NSString *tmp;
    
    tmp = [user objectForKey:key];
    
    if ([tmp isEqualToString:obj]) {
        isObjInCurrentUser=YES;
    }
    
    NSLog(@"Current user:%@ and %@", tmp, obj);
    
    return isObjInCurrentUser;
}

- (BOOL)isUserVerified
{
    BOOL isUserVerified = NO;
    PFUser *user = [PFUser currentUser];
    
    isUserVerified = [[user objectForKey:@"emailVerified"] boolValue];
    
//    if ([[user objectForKey:@"emailVerified"] boolValue]){
//        isUserVerified = YES;
//        // Refresh to make sure the user did not recently verify
//        [user refresh];
//        if (![[user objectForKey:@"emailVerified"] boolValue]) {
//            isUserVerified = NO;
//        } else {
//            isUserVerified = YES;
//        }
//        NSLog(@"vrf");
//    }
    
    return isUserVerified;
}

- (BOOL)isObjExistInUser:(NSString*)obj inKey:(NSString*)key
{
    BOOL isObjExist = YES;
    NSMutableArray *tmpQuery = [[NSMutableArray alloc] init];
    
    NSInteger numOfObj;
    
    //"user" is spical object in parse
    //use [PFUser query]
    
    PFQuery *query = [PFUser query];
    [query whereKey:key equalTo:obj];
    tmpQuery = [[query findObjects] mutableCopy];
    
    numOfObj = tmpQuery.count;
    if (numOfObj == 0) {
        isObjExist = NO;
    }
    NSLog(@"%ld and %@", (long)numOfObj, obj);
    
    return isObjExist;
}

- (void)recoverUserPasswordWithEmail:(NSString*)email
{
    [PFUser requestPasswordResetForEmailInBackground:email];
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

@end
