//
//  User.h
//  My Happy Plate
//
//  Created by JoshJSZ on 6/13/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *passWord;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSMutableArray *allergonList;

//- (BOOL)isUserLoggedIn:(NSUserDefaults *)userDefaults;
- (BOOL)isUserLoggedIn;

- (BOOL)logInNewUser;

- (void)logOut;

- (BOOL)isObjExistInUser:(NSString*)obj inKey:(NSString*)key;

- (BOOL)isObjInCurrentUser:(NSString*)obj inKey:(NSString*)key;

- (BOOL)regNewUser;

- (BOOL)isUserVerified;

- (BOOL)updateEmail;

- (void)recoverUserPasswordWithEmail:(NSString*)email;
- (void)fetchCurrentUserInfo;

@end
