//
//  RegPersonalInfoTableViewController.m
//  My Happy Plate
//
//  Created by JoshJSZ on 6/17/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "RegPersonalInfoTableViewController.h"

@interface RegPersonalInfoTableViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UILabel *errorMessageLable;
@property (weak, nonatomic) IBOutlet UIButton *firstNameButton;
@property (weak, nonatomic) IBOutlet UIButton *lastNameButton;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;

@end

@implementation RegPersonalInfoTableViewController

@synthesize user;

- (IBAction)touchNextButton:(UIBarButtonItem *)sender {
    [self validateTextFields];
    if (self.errorMessageLable.text.length == 0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"regLogInInfo"];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.firstNameTextField becomeFirstResponder];
    user = [CurrentUserObject currentUserObject];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 3;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - - handel text-button (better text input)
- (IBAction)touchInButton:(UIButton *)sender {
    if (sender == self.firstNameButton) {
        [self.firstNameTextField becomeFirstResponder];
    } else if (sender == self.lastNameButton) {
        [self.lastNameTextField becomeFirstResponder];
    } else if (sender == self.emailButton) {
        [self.emailTextField becomeFirstResponder];
    }
}

- (IBAction)valueChangedInTextField:(UITextField *)sender {
    if (sender == self.firstNameTextField) {
        [self setButtonTextNormal:self.firstNameButton];

	}
	else if (sender == self.lastNameTextField) {
        [self setButtonTextNormal:self.lastNameButton];

	}
	else if (sender == self.emailTextField) {
        [self setButtonTextNormal:self.emailButton];
        sender.text = sender.text.lowercaseString;
    }
}

#pragma mark - Helper func for textFields

- (void)setButtonTextToRedAndBold:(UIButton*)button
{
    [button setTitleColor:[UIColor colorWithRed:0.502 green:0.0 blue:0.0 alpha:1] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:15];
}

- (void)setButtonTextNormal:(UIButton*)button
{
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:15];
}


#pragma mark - - keybord "next" and "Ok" button

- (IBAction)touchNextOnKeyboard:(UITextField *)sender {
    if (sender == self.firstNameTextField) {
		[sender resignFirstResponder];
		[self.lastNameTextField becomeFirstResponder];
	}
	else if (sender == self.lastNameTextField) {
		[sender resignFirstResponder];
		[self.emailTextField becomeFirstResponder];
	}
	else if (sender == self.emailTextField) {
        [self validateTextFields];
        
        if (self.errorMessageLable.text.length == 0) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"regLogInInfo"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
	}
}


#pragma mark - vaildate input

- (void)validateTextFields
{
    self.errorMessageLable.text = @"";
    
    //Check firstNameTextField
    if (self.firstNameTextField.text.length) {
        self.user.firstName = self.firstNameTextField.text;
    } else {
        self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:@"First Name can't be empty\n"];
        [self setButtonTextToRedAndBold:self.firstNameButton];
    }
    
    //Check lastNameTextField
    if (self.lastNameTextField.text.length) {
        self.user.lastName = self.lastNameTextField.text;
    } else {
        self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:@"Last Name can't be empty\n"];
        [self setButtonTextToRedAndBold:self.lastNameButton];
    }
    
    //Check emailTextField
    if (self.emailTextField.text.length) {
        
        //Run if emailTextField is not empty
        if ([self validateEmailWithString:self.emailTextField.text]) {
            
            //Run if emailTextField is a vaild email address
            if ([self.user isObjExistInUser:self.emailTextField.text inKey:@"email"]) {
                self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:@"Email already exist, pick another one?\n"];
                [self setButtonTextToRedAndBold:self.emailButton];
            } else {
                self.user.email = self.emailTextField.text;
            }
        } else {
            //Else, Run if emailTextField is not a vaild email address
            self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:@"Please enter a valid Email address\n"];
            [self setButtonTextToRedAndBold:self.emailButton];
        }
    } else {
        //Else, Run if emailTextField is empty
        self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:@"Email can't be empty\n"];
        [self setButtonTextToRedAndBold:self.emailButton];
    }
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

@end
