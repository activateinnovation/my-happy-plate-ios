//
//  LoginViewViewController.m
//  My Happy Plate
//
//  Created by JoshJSZ on 6/13/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "LoginViewViewController.h"

@interface LoginViewViewController () <UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UIView *logInAndImage;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passWordTextField;
@property (weak, nonatomic) IBOutlet UILabel *errorLable;

@end

@implementation LoginViewViewController

@synthesize userDefaults;
@synthesize user;

- (IBAction)touchLogInNewUser:(UIButton *)sender
{
    if (![self isUserNamePasswordEmpty]) {
        if ([self logInNewUser]) {
            if ([self.user isUserVerified]) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                
                UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
                [self presentViewController:viewController animated:YES completion:nil];
            } else {
                [self.user logOut];
                self.errorLable.text = @"Please verify your Email";
            }
        } else {
            self.errorLable.text = @"Invalid User Name or Password";
        }
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    //code to recive notification from the keyboard
    [self handelKeyboardNotification];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    [self keyboardWillDisappear];
}

-(void)keyboardWillAppear {
    CGRect frame = self.logInAndImage.frame;
    if (frame.origin.y != -40.0) {
        [self moveLogInViewUp:YES];
    }
}

-(void)keyboardWillDisappear {
    CGRect frame = self.logInAndImage.frame;
    if (frame.origin.y != 0.0) {
        [self moveLogInViewUp:NO];
    }
}

-(void)moveLogInViewUp:(BOOL)beMoveUp{
    [UIView beginAnimations:nil context:NULL];
    CGRect frame = self.logInAndImage.frame;
    if (beMoveUp) {
        NSLog(@"up:%f", frame.origin.y);
            [UIView setAnimationDuration:0.3]; // to slide the view up
            frame.origin.y = -40.0;
    } else {
        NSLog(@"down:%f", frame.origin.y);
            [UIView setAnimationDuration:0.15]; // to slide the view up
            frame.origin.y = 0.0;
    }
    
    self.logInAndImage.frame = frame;
    
    [UIView commitAnimations];
}
- (IBAction)touchNeedHelpButton:(UIButton *)sender {
    [self displayActionSheet];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - - keybord "next" and "Ok" button

- (IBAction)touchNextOnKeyboard:(UITextField *)sender {
    if (sender == self.userNameTextField) {
		[sender resignFirstResponder];
		[self.passWordTextField becomeFirstResponder];
	}
	else if (sender == self.passWordTextField) {
		[sender resignFirstResponder];
        
        if (![self isUserNamePasswordEmpty]) {
            if ([self logInNewUser]) {
                if ([self.user isUserVerified]) {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    
                    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
                    [self presentViewController:viewController animated:YES completion:nil];
                } else {
                    [self.user logOut];
                    self.errorLable.text = @"Please verify your Email";
                }
            } else {
                self.errorLable.text = @"Invalid User Name or Password";
            }
        }
	}
}

#pragma mark - handelKeyboardNotification
- (void)handelKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillAppear)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
}

#pragma mark - log in user
- (BOOL)logInNewUser
{
    BOOL isUserLoggedIn = NO;
    
    user = [CurrentUserObject currentUserObject];
    
    self.user.userName = self.userNameTextField.text;
    self.user.passWord = self.passWordTextField.text;
    
    isUserLoggedIn = [self.user logInNewUser];
    
    return isUserLoggedIn;
}

- (BOOL)isUserNamePasswordEmpty
{
    BOOL isUserNamePasswordEmpty = NO;
    
    self.errorLable.text = @"";
    if (self.userNameTextField.text.length == 0) {
        self.errorLable.text = [self.errorLable.text stringByAppendingString:@"User Name - "];
        isUserNamePasswordEmpty = YES;
    }
    if (self.passWordTextField.text.length == 0) {
        self.errorLable.text = [self.errorLable.text stringByAppendingString:@"Password - "];
        isUserNamePasswordEmpty = YES;
    }
    if (isUserNamePasswordEmpty) {
        self.errorLable.text = [self.errorLable.text stringByAppendingString:@"Can't be empty"];
    }
    return isUserNamePasswordEmpty;
}

# pragma mark - action sheet

- (void)displayActionSheet
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Recover My Password" otherButtonTitles:
                            @"Re-send Email Conformation",
                            nil];
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}
//change action sheet's font
- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
    [actionSheet.subviews enumerateObjectsUsingBlock:^(id _currentView, NSUInteger idx, BOOL *stop) {
        if ([_currentView isKindOfClass:[UIButton class]]) {
            [((UIButton *)_currentView).titleLabel setFont:[UIFont systemFontOfSize:18.f]];
            // OR
            //[((UIButton *)_currentView).titleLabel setFont:[UIFont fontWithName:@"Exo2-SemiBold" size:17]];
        }
    }];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"recoverPassword"];
        [self.navigationController pushViewController:viewController animated:YES];
    } else if (buttonIndex == 1) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"emailVerification"];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

@end
