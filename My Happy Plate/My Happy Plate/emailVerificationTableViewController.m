//
//  emailVerificationTableViewController.m
//  My Happy Plate
//
//  Created by JoshJSZ on 6/20/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "emailVerificationTableViewController.h"

@interface emailVerificationTableViewController () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UILabel *errorMessageLable;
@property (weak, nonatomic) IBOutlet UIButton *userNameButton;
@property (weak, nonatomic) IBOutlet UIButton *passwordButton;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sendButton;



@end

@implementation emailVerificationTableViewController

@synthesize user;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    user = [CurrentUserObject currentUserObject];
    [self.userNameTextField becomeFirstResponder];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 3;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - touch send button
- (IBAction)touchSendButton:(UIBarButtonItem *)sender {
    [self validateTextFields];
    if (self.errorMessageLable.text.length == 0) {
        self.errorMessageLable.textColor = [UIColor blackColor];
        self.errorMessageLable.text = @"Loading...";
        [self postAlert];
    }
}

-(void)postAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                    message:[NSString stringWithFormat:@"Are you sure you want to associate your account with \"%@\"", self.emailTextField.text]
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}


#pragma mark - handel entery press
- (IBAction)touchInButton:(UIButton *)sender {
    if (sender == self.userNameButton) {
        [self.userNameTextField becomeFirstResponder];
    } else if (sender == self.passwordButton) {
        [self.passwordTextField becomeFirstResponder];
    } else if (sender == self.emailButton) {
        [self.emailTextField becomeFirstResponder];
    }
}

- (IBAction)valueChangedInTextField:(UITextField *)sender {
    if (sender == self.userNameTextField) {
        [self setButtonTextNormal:self.userNameButton];
        
	}
	else if (sender == self.passwordTextField) {
        [self setButtonTextNormal:self.passwordButton];
        
	}
	else if (sender == self.emailTextField) {
        [self setButtonTextNormal:self.emailButton];
        sender.text = sender.text.lowercaseString;
    }
}

#pragma mark - Helper func for textFields

- (void)setButtonTextToRedAndBold:(UIButton*)button
{
    [button setTitleColor:[UIColor colorWithRed:0.502 green:0.0 blue:0.0 alpha:1] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:15];
}

- (void)setButtonTextNormal:(UIButton*)button
{
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:15];
}

- (void)appendErrorMessage:(NSString *)message relatedButton:(UIButton*)button
{
    self.errorMessageLable.textColor = [UIColor colorWithRed:0.5 green:0.0 blue:0.0 alpha:1];
    self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:message];
    [self setButtonTextToRedAndBold:button];
}

#pragma mark - hendel press next on keyboard

- (IBAction)nextOnKeyboard:(id)sender {
    
    if (sender == self.userNameTextField) {
		[sender resignFirstResponder];
		[self.passwordTextField becomeFirstResponder];
	}
	else if (sender == self.passwordTextField) {
		[sender resignFirstResponder];
		[self.emailTextField becomeFirstResponder];
	}
	else if (sender == self.emailTextField) {
		[sender resignFirstResponder];
        [self validateTextFields];
        if (self.errorMessageLable.text.length == 0) {
            self.errorMessageLable.textColor = [UIColor blackColor];
            self.errorMessageLable.text = @"Loading...";
            [self postAlert];
        }
	}
    
}

#pragma mark - check text fields
- (void)validateTextFields
{
    self.errorMessageLable.text = @"";
    
    //check userNameTextField
    if (self.userNameTextField.text.length >= 5) {
        self.user.userName = self.userNameTextField.text;
    } else if (self.userNameTextField.text.length) {
        [self appendErrorMessage:@"User name must be least 5 characters\n"
                   relatedButton:self.userNameButton];
    } else {
        [self appendErrorMessage:@"Please enter a User Name\n"
                   relatedButton:self.userNameButton];
    }
    
    //check passwordTextField
    if (self.passwordTextField.text.length >= 6) {
        self.user.passWord = self.passwordTextField.text;
    }else if (self.passwordTextField.text.length == 0) {
        [self appendErrorMessage:@"Please enter a Password\n" relatedButton:self.passwordButton];
    } else if (self.passwordTextField.text.length < 6){
        [self appendErrorMessage:@"Password must be least 6 characters\n" relatedButton:self.passwordButton];
    }
    
    //check email
    if (self.emailTextField.text.length != 0) {
        if ([self validateEmailWithString:self.emailTextField.text]) {
            
        } else {
            [self appendErrorMessage:@"Please enter a valid Email address" relatedButton:self.emailButton];
        }

    } else {
        [self appendErrorMessage:@"Please enter a Email" relatedButton:self.emailButton];
    }

}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

#pragma mark - UIAlertViewDelegate
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    //u need to change 0 to other value(,1,2,3) if u have more buttons.then u can check which button was pressed.
    
    if (buttonIndex == 1) {
        self.errorMessageLable.text = @"";
        if ([self logInNewUser]) {
            if ([self.user isObjInCurrentUser:self.emailTextField.text inKey:@"email"] ^
                [self.user isObjExistInUser:self.emailTextField.text inKey:@"email"]) {
                [self appendErrorMessage:@"Email already associated with another account\n" relatedButton:self.emailButton];
                if ([self.user isUserLoggedIn]) {
                    [self.user logOut];
                }
            } else {
                [self resendEmail];
            }
        } else {
            [self appendErrorMessage:@"Invalid User Name or Password\n" relatedButton:self.userNameButton];
            [self setButtonTextToRedAndBold:self.passwordButton];
            if ([self.user isUserLoggedIn]) {
                [self.user logOut];
            }
        }
    } else {
        self.errorMessageLable.text = @"";
    }
}

#pragma mark - actual resend email function
- (void)resendEmail
{
    if ([self.user isUserLoggedIn]) {
        self.user.email = self.emailTextField.text;
        if ([self.user updateEmail]) {
            [self.user logOut];
            self.errorMessageLable.textColor = [UIColor colorWithRed:0.0 green:0.5 blue:0.25 alpha:1];
            self.errorMessageLable.text = [NSString stringWithFormat:@"Success!\nEmail sent to %@\nPlease check your email to verify your account", self.emailTextField.text];
            [self disableInput];
        } else {
            self.errorMessageLable.text = @"Error while Updating Email, Please Retry";
        }
    } else {
        self.errorMessageLable.text = @"No user Logged In, Please Retry";
    }
}

-(void)disableInput
{
    self.userNameButton.enabled = NO;
    self.userNameTextField.enabled = NO;
    self.passwordButton.enabled = NO;
    self.passwordTextField.enabled = NO;
    self.emailButton.enabled = NO;
    self.emailTextField.enabled = NO;
    self.sendButton.enabled = NO;
}

#pragma mark - log in user
- (BOOL)logInNewUser
{
    BOOL isUserLoggedIn = NO;
    
    user = [CurrentUserObject currentUserObject];
    
    self.user.userName = self.userNameTextField.text;
    self.user.passWord = self.passwordTextField.text;
    
    isUserLoggedIn = [self.user logInNewUser];
    
    return isUserLoggedIn;
}


@end
