//
//  RecoverPasswordTableViewController.m
//  My Happy Plate
//
//  Created by JoshJSZ on 6/24/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "RecoverPasswordTableViewController.h"

@interface RecoverPasswordTableViewController () <UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UILabel *errorMessageLable;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sendButton;

@end

@implementation RecoverPasswordTableViewController

@synthesize user;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.emailTextField becomeFirstResponder];
    user = [CurrentUserObject currentUserObject];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 1;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - touch send button
- (IBAction)touchSendButton:(UIBarButtonItem *)sender {
    [self validateTextFields];
    if (self.errorMessageLable.text.length == 0) {
        self.errorMessageLable.textColor = [UIColor blackColor];
        self.errorMessageLable.text = @"Loading...";
        [self postAlert];
    }
}

#pragma mark - touch email button
- (IBAction)touchEmailButton:(UIButton *)sender {
    [self.emailTextField becomeFirstResponder];
}

#pragma mark - hendel press next on keyboard
- (IBAction)touchNextOnKeyboard:(UITextField *)sender {
    [self validateTextFields];
    if (self.errorMessageLable.text.length == 0) {
        self.errorMessageLable.textColor = [UIColor blackColor];
        self.errorMessageLable.text = @"Loading...";
        [self postAlert];
    }
}

#pragma mark - validateTextFields
- (void)validateTextFields
{
    self.errorMessageLable.text = @"";
    self.errorMessageLable.textColor = [UIColor colorWithRed:0.5 green:0.0 blue:0.0 alpha:1];
    
    //Check emailTextField
    if (self.emailTextField.text.length) {
        
        //Run if emailTextField is not empty
        if (![self validateEmailWithString:self.emailTextField.text]) {

            //Run if emailTextField is not a vaild email address
            self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:@"Please enter a valid Email address\n"];
            [self setButtonTextToRedAndBold:self.emailButton];
        }
    } else {
        //Else, Run if emailTextField is empty
        self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:@"Email can't be empty\n"];
        [self setButtonTextToRedAndBold:self.emailButton];
    }
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (IBAction)valueChangedInTextField:(UITextField *)sender {
    [self setButtonTextNormal:self.emailButton];
    sender.text = sender.text.lowercaseString;
}

#pragma mark - Helper func for textFields

- (void)setButtonTextToRedAndBold:(UIButton*)button
{
    [button setTitleColor:[UIColor colorWithRed:0.502 green:0.0 blue:0.0 alpha:1] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:15];
}

- (void)setButtonTextNormal:(UIButton*)button
{
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:15];
}

#pragma mark - UIAlertViewDelegate
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    //u need to change 0 to other value(,1,2,3) if u have more buttons.then u can check which button was pressed.
    
    if (buttonIndex == 1) {
        if ([self.user isObjExistInUser:self.emailTextField.text inKey:@"email"]) {
            [self recoverPassword];
        } else {
            self.errorMessageLable.textColor = [UIColor colorWithRed:0.5 green:0.0 blue:0.0 alpha:1];
            self.errorMessageLable.text = @"Email dose not exist on our record\n";
            [self setButtonTextToRedAndBold:self.emailButton];
        }
    } else {
        self.errorMessageLable.text = @"";
    }
    
}

-(void)postAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                    message:[NSString stringWithFormat:@"Are you sure you want to recover your password using \"%@\" ?", self.emailTextField.text]
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}


#pragma mark - the actual recover password method

- (void)recoverPassword
{
    [self.user recoverUserPasswordWithEmail:self.emailTextField.text];
    self.errorMessageLable.textColor = [UIColor colorWithRed:0.0 green:0.5 blue:0.25 alpha:1];
    self.errorMessageLable.text = [NSString stringWithFormat:@"Success!\nEmail sent to %@\nPlease check your email to reset your password", self.emailTextField.text];
    self.emailTextField.enabled = NO;
    self.emailButton.enabled = NO;
    self.sendButton.enabled = NO;
    
}

@end
