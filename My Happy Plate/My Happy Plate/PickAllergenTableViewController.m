//
//  PickAllergenTableViewController.m
//  My Happy Plate
//
//  Created by JoshJSZ on 6/17/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "PickAllergenTableViewController.h"

@interface PickAllergenTableViewController () <UIAlertViewDelegate>

@property (strong, nonatomic) NSMutableArray *selectedRow;

@end

@implementation PickAllergenTableViewController

@synthesize user;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.allAllergen = [[AllAllergon alloc] init];
    self.selectedRow = [[NSMutableArray alloc] init];
    
    self.localAllAllergen = [self.allAllergen allAllergonList];
    user = [CurrentUserObject currentUserObject];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    NSInteger numberOfRows = 0;
    numberOfRows = self.localAllAllergen.count;
    return numberOfRows;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"AllAllergen Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = [self.localAllAllergen objectAtIndex:indexPath.row];
    if (self.selectedRow.count) {
        if ([self.selectedRow containsObject:[NSString stringWithFormat:@"%ld", (long)indexPath.row]]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.textLabel.font = [UIFont boldSystemFontOfSize:18];
            cell.textLabel.textColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1];
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.textLabel.font = [UIFont systemFontOfSize:18];
            cell.textLabel.textColor = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1];
        }
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.font = [UIFont systemFontOfSize:18];
        cell.textLabel.textColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1];
    }

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.selectedRow containsObject:[NSString stringWithFormat:@"%ld", (long)indexPath.row]]) {
        [self.selectedRow removeObject:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
    } else {
        [self.selectedRow addObject:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
    }
    NSLog(@"%@", self.selectedRow);
//    NSArray *array = [NSArray arrayWithObject:indexPath];
//    [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
    [tableView reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)touchDone:(UIBarButtonItem *)sender {
    if (self.selectedRow.count) {
        for (NSInteger i=0; i<self.selectedRow.count; i++) {
            [self.user.allergonList addObject:[NSString stringWithFormat:@"%@",[self.localAllAllergen objectAtIndex:[[self.selectedRow objectAtIndex:i] intValue]]]];
        }
        
        if(self.user.regNewUser){
            [self postSuccessAlert];
        }
    }
    else {
        [self postAlert];
    }
}

-(void)postAlert{
    UIAlertView *alertNoAllergen = [[UIAlertView alloc] initWithTitle:@"Please Pick at least One Allergen"
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil, nil];
    alertNoAllergen.tag = 0;
    [alertNoAllergen show];
}

-(void)postSuccessAlert{
    UIAlertView *alertSuccess = [[UIAlertView alloc] initWithTitle:@"Your Account was Created Successfully!"
                                                    message:@"Please verify your email before you Login"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil, nil];
    alertSuccess.tag = 1;
    [alertSuccess show];
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    //u need to change 0 to other value(,1,2,3) if u have more buttons.then u can check which button was pressed.
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"LogInNav"];
            [self presentViewController:viewController animated:YES completion:nil];
        }
    }
    
}

@end
