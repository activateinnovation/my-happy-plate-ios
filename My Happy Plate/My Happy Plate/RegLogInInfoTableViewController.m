//
//  RegLogInInfoTableViewController.m
//  My Happy Plate
//
//  Created by JoshJSZ on 6/17/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "RegLogInInfoTableViewController.h"

@interface RegLogInInfoTableViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *verifyPasswordTextField;
@property (weak, nonatomic) IBOutlet UILabel *errorMessageLable;
@property (weak, nonatomic) IBOutlet UIButton *userNameButton;
@property (weak, nonatomic) IBOutlet UIButton *passwordButton;
@property (weak, nonatomic) IBOutlet UIButton *verifyPasswordButton;


@end

@implementation RegLogInInfoTableViewController

@synthesize user;

- (IBAction)touchNextButton:(UIBarButtonItem *)sender {
    [self validateTextFields];
    
    if (self.errorMessageLable.text.length == 0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"pickAllergrn"];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.userNameTextField becomeFirstResponder];
    user = [CurrentUserObject currentUserObject];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 3;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - - handel text-button (better text input)

- (IBAction)touchInButton:(UIButton *)sender {
    if (sender == self.userNameButton) {
        [self.userNameTextField becomeFirstResponder];
    } else if (sender == self.passwordButton) {
        [self.passwordTextField becomeFirstResponder];
    } else if (sender == self.verifyPasswordButton) {
        [self.verifyPasswordTextField becomeFirstResponder];
    }
}

- (IBAction)valueChangedInTextField:(UITextField *)sender {
    if (sender == self.userNameTextField) {
        [self setButtonTextNormal:self.userNameButton];
        
	}
	else if (sender == self.passwordTextField) {
        [self setButtonTextNormal:self.passwordButton];
        
	}
	else if (sender == self.verifyPasswordTextField) {
        [self setButtonTextNormal:self.verifyPasswordButton];
        
    }
}

#pragma mark - Helper func for textFields

- (void)setButtonTextToRedAndBold:(UIButton*)button
{
    [button setTitleColor:[UIColor colorWithRed:0.502 green:0.0 blue:0.0 alpha:1] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:15];
}

- (void)setButtonTextNormal:(UIButton*)button
{
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:15];
}

#pragma mark - - keybord "next" and "Ok" button


- (IBAction)nextOnKeyboard:(id)sender {
    
    if (sender == self.userNameTextField) {
		[sender resignFirstResponder];
		[self.passwordTextField becomeFirstResponder];
	}
	else if (sender == self.passwordTextField) {
		[sender resignFirstResponder];
		[self.verifyPasswordTextField becomeFirstResponder];
	}
	else if (sender == self.verifyPasswordTextField) {
		[sender resignFirstResponder];
        [self validateTextFields];
        
        if (self.errorMessageLable.text.length == 0) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"pickAllergrn"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
	}
    
}

#pragma mark - - Validate Text Fields Input

- (void)validateTextFields
{
    self.errorMessageLable.text = @"";
    
    //check userNameTextField
    if (self.userNameTextField.text.length >= 5) {
        if ([self.user isObjExistInUser:self.userNameTextField.text inKey:@"username"]) {
            self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:@"User Name already exist, pick another one?\n"];
                [self setButtonTextToRedAndBold:self.userNameButton];
        } else {
            self.user.userName = self.userNameTextField.text;
        }
    } else if (self.userNameTextField.text.length) {
        self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:@"Try at least 5 characters for User Name\n"];
        [self setButtonTextToRedAndBold:self.userNameButton];
    } else {
        self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:@"User Name can't be empty\n"];
        [self setButtonTextToRedAndBold:self.userNameButton];
    }
    
    //check passwordTextField
    if (self.passwordTextField.text.length == 0) {
        self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:@"Password can't be empty\n"];
        [self setButtonTextToRedAndBold:self.passwordButton];
    } else if (self.passwordTextField.text.length < 6){
        self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:@"Try at least 6 characters for Password\n"];
        [self setButtonTextToRedAndBold:self.passwordButton];
    }
    
    //check verifyPasswordTextField
    if (self.verifyPasswordTextField.text.length) {
        if ([self.passwordTextField.text isEqualToString:self.verifyPasswordTextField.text]) {
            self.user.passWord = self.passwordTextField.text;
        } else {
            self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:@"Verify password dose not match\n"];
            [self setButtonTextToRedAndBold:self.verifyPasswordButton];
        }
    } else {
        self.errorMessageLable.text = [self.errorMessageLable.text stringByAppendingString:@"Please verify password\n"];
        [self setButtonTextToRedAndBold:self.verifyPasswordButton];
    }
}

@end
