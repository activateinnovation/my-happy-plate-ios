//
//  ViewController.m
//  My Happy Plate
//
//  Created by JoshJSZ on 6/13/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *welcomLable;

@end

@implementation ViewController

@synthesize userDefaults;
@synthesize user;

- (IBAction)touchLogOut:(UIButton *)sender {
    user = [CurrentUserObject currentUserObject];
    [self.user logOut];
    [self updateUserLogInStatus];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

-(void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    //userDefaults = [NSUserDefaults standardUserDefaults];
    //BOOL loggedIn = NO;
    //user = [CurrentUserObject currentUserObject];
    
    //loggedIn = [self.user isUserLoggedIn:userDefaults];
    //if(loggedIn){
    [self updateUserLogInStatus];
    [self displayWelcomeMessage];

}

- (void)updateUserLogInStatus
{
    user = [CurrentUserObject currentUserObject];
    if([self.user isUserLoggedIn]){
        [user fetchCurrentUserInfo];
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"LogInNav"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
}

- (void)displayWelcomeMessage
{
    self.welcomLable.text = [NSString stringWithFormat:@"Welcome\n\n%@ %@ - %@\n%@", user.firstName, user.lastName, user.userName, user.email];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
