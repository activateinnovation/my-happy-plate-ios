//
//  LoginViewViewController.h
//  My Happy Plate
//
//  Created by JoshJSZ on 6/13/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "AllAllergon.h"
#import "CurrentUserObject.h"

@interface LoginViewViewController : UIViewController

@property (strong, nonatomic) NSUserDefaults *userDefaults;
@property (strong, nonatomic) User *user;

@end
